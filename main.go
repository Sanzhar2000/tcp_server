package main

import (
	"fmt"
	"math"
	"net"
	"strconv"
)

func main() {
	listener, err := net.Listen("tcp", ":4545")


	if err != nil {
		fmt.Println(err)
		return
	}
	defer listener.Close()
	fmt.Println("Server is listening...")

	const maxClients = 10
	sema := make(chan struct{}, maxClients)

	for {
		sema <- struct{}{}
		defer func() { <-sema }()

		conn, err := listener.Accept()
		if err != nil {
			fmt.Println(err)
			conn.Close()
			continue
		}

		go handleConnection(conn)  // запускаем горутину для обработки запроса
	}
}
// обработка подключения
func handleConnection(conn net.Conn) {
	defer conn.Close()
	for {
		// считываем полученные в запросе данные
		input := make([]byte, (1024 * 4))
		n, err := conn.Read(input)
		if n == 0 || err != nil {
			fmt.Println("Read error:", err)
			break
		}
		source := string(input[0:n])
		//fmt.Println(source)

		number, err := strconv.Atoi(source) //ковертируем строку в число
		//fmt.Println(number)
		if err != nil {
			fmt.Println("Input not an integer:", err)
			break
		}

		x := int(math.Pow(float64(number), 2)) // возведем во 2 степень
		//fmt.Println(x)

		s := strconv.Itoa(x)
		//fmt.Println(s)

		conn.Write([]byte(s)) // отправляем данные клиенту
	}
}